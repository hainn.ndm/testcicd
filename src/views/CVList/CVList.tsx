import React, {ComponentType, useEffect, useState} from "react";
import {createStyles, Theme, withStyles} from "@material-ui/core/styles";
import {compose} from "recompose";
import Page from "components/Page/Page";
import ToolbarManager from "components/ToolbarManager/ToolbarManager";
import {debounce} from "lodash";
import TableManager from "views/CVList/components/TableManager/TableManager";
import {graphqlFilter} from "helpers/string";
import palette from "theme/palette";
import { API } from "utils/api";
import { axios } from "utils/axiosInstance";

interface IProps {
  classes?: any;
}

const styles = (theme: Theme) =>
  createStyles({
    root: {
      marginTop: theme.spacing(2),
      padding: theme.spacing(3),
    },
    content: {
      padding: 0,
    },
    inner: {
      // minWidth: 1050
    },
    nameCell: {
      display: "flex",
      alignItems: "center",
    },
    avatar: {
      height: 42,
      width: 42,
      marginRight: theme.spacing(1),
      borderRadius: "50%",
    },
    actions: {
      padding: theme.spacing(0, 1),
      justifyContent: "flex-end",
    },
    tableRow: {},
    view: {
      width: theme.spacing(3),
      height: theme.spacing(3),
      backgroundColor: palette.primary.main,
      margin: "0px 4px",
      cursor: "pointer",
    },
    icon: {
      color: "#fff",
      fontSize: 16,
    },
    logo: {
      objectFit: "cover",
      width: 50,
      height: 50,
      borderRadius: "50%",
    },
    title: {
      minWidth: 250,
      [theme.breakpoints.down("xs")]: {
        minWidth: "calc(100vw - 210px)",
      },
    },
  });

const CVList: ComponentType<IProps> = (props: IProps) => {
  const {classes} = props;
  const [datas, setDatas] = useState<any>([]);
  const [countDatas, setCountDatas] = useState<number>();
  const [filter, setFilter] = useState("{}");

  useEffect(() => {
    let filterString = graphqlFilter({});
    fetchDatas(1, 10, filterString);
  }, []);

  const fetchDatas = async (page: number = 1, limit: number = 10, filter: string = "{}") => {
    try {
      let r = await axios.get(`${API.GET_CV_LIST}?offset=${page}&limit=${limit}`)
      setDatas(r.data.data)
      setCountDatas(r.data.total_page * limit)
    } catch (error) {
      console.log(error)
    }
  };

  const onSearch = debounce((keyword: string = "{}") => {
    let filterString = graphqlFilter({name: keyword});
    setFilter(filterString);
    fetchDatas(1, 10, filterString);
  }, 300);

  const handleChangePage = (page: number = 1) => {
    fetchDatas(page + 1, 10, filter);
  };

  return (
    <Page title={"Danh CV ứng viên"} heading={"Danh sách CV ứng viên"} className={classes.root}>
      <ToolbarManager onSearch={onSearch}/>
      <div className={classes.content}>
        <TableManager
          datas={datas}
          countDatas={countDatas}
          onChangePage={handleChangePage}
        />
      </div>
    </Page>
  );
};

export default compose<IProps, any>(withStyles(styles))(CVList);
