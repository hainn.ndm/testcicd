import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Card, Grid, colors, Typography } from "@material-ui/core";
import { apolloClient } from "utils/apolloClient";
import { COMPANY_COUNT } from "graphql/company/query";
import { JOB_POST_COUNT } from "graphql/job_post/query";
import { USERS_COUNT } from "graphql/user/query";

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  content: {
    padding: 0,
  },
  item: {
    padding: theme.spacing(3),
    textAlign: "center",
    [theme.breakpoints.up("md")]: {
      "&:not(:last-of-type)": {
        borderRight: `1px solid ${theme.palette.divider}`,
      },
    },
    [theme.breakpoints.down("sm")]: {
      "&:not(:last-of-type)": {
        borderBottom: `1px solid ${theme.palette.divider}`,
      },
    },
  },
  titleWrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  label: {
    marginLeft: theme.spacing(1),
    backgroundColor: colors.green[600],
    color: "#fff",
    padding: "4px 8px",
    fontWeight: 500,
    borderRadius: 5,
  },
  overline: {
    marginTop: theme.spacing(1),
  },
}));

interface StatisticsProps {
  className?: string;
}

const Statistics: React.FunctionComponent<StatisticsProps> = (props: StatisticsProps) => {
  const { className, ...rest } = props;

  const classes = useStyles();

  const [totalCompany, setTotalCompany] = useState(0);
  const [totalJobPost, setTotalJobPost] = useState(0);
  const [totalUser, setTotalUser] = useState(0);
  const [watchingNow, setWatchingNow] = useState(0);

  useEffect(() => {
    Promise.all([fetchCompany(), fetJobPost(), fetJobUser()]);
  }, []);

  const fetchCompany = () => {
    apolloClient
      .query({ query: COMPANY_COUNT, variables: { page: 1, limit: 1, filter: "{}" } })
      .then((r) => setTotalCompany(r.data.companys.pageInfo.length));
  };

  const fetJobPost = () => {
    apolloClient
      .query({ query: JOB_POST_COUNT, variables: { page: 1, limit: 1, filter: "{}" } })
      .then((r) => setTotalJobPost(r.data.jobPosts.pageInfo.length));
  };

  const fetJobUser = () => {
    apolloClient
      .query({ query: USERS_COUNT, variables: { page: 1, limit: 1, filter: "{}" } })
      .then((r) => setTotalUser(r.data.users.pageInfo.length));
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <Grid alignItems="center" container justify="space-between">
        <Grid className={classes.item} item md={3} sm={6} xs={12}>
          <a href={`#`}>
            <div>
              <Typography variant="h4">{totalCompany}</Typography>
            </div>
            <Typography className={classes.overline} variant="overline">
              Tổng số công ty
            </Typography>
          </a>
        </Grid>
        <Grid className={classes.item} item md={3} sm={6} xs={12}>
          <a href={`#`}>
            <div>
              <Typography variant="h4">{totalJobPost}</Typography>
            </div>
            <Typography className={classes.overline} variant="overline">
              Tổng số tin rao
            </Typography>
          </a>
        </Grid>
        <Grid className={classes.item} item md={3} sm={6} xs={12}>
          <a href={`#`}>
            <div>
              <Typography variant="h4">{totalUser}</Typography>
            </div>

            <Typography className={classes.overline} variant="overline">
              Tổng số người dùng
            </Typography>
          </a>
        </Grid>

        <Grid className={classes.item} item md={3} sm={6} xs={12}>
          <a href={`#`}>
            <div className={classes.titleWrapper}>
              <Typography variant="h4">{watchingNow}</Typography>
              <div className={classes.label}>
                <p>Live</p>
              </div>
            </div>

            <Typography className={classes.overline} variant="overline">
              Đang online
            </Typography>
          </a>
        </Grid>
      </Grid>
    </Card>
  );
};

export default Statistics;
