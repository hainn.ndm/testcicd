import React, { useEffect, useState } from "react";
import clsx from "clsx";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Typography, Grid, Hidden } from "@material-ui/core";
import moment from "moment";
import Image from "assets/images/overview.svg";

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  summaryButton: {
    backgroundColor: "white",
  },
  barChartIcon: {
    marginRight: theme.spacing(1),
  },
  image: {
    width: "100%",
    maxHeight: 400,
  },
}));

interface HeaderProps {
  className?: string;
}

const Header: React.FunctionComponent<HeaderProps> = (props: HeaderProps) => {
  const { className, ...rest } = props;

  const classes = useStyles();

  const [quote, setQuote] = useState("");

  const quotes = [
    "Success is not final; failure is not fatal: It is the courage to continue that counts.",
    "Success usually comes to those who are too busy to be looking for it.",
    "Opportunities don't happen. You create them.",
    "I find that the harder I work, the more luck I seem to have.",
    "Try not to become a man of success. Rather become a man of value.",
    "Don't let the fear of losing be greater than the excitement of winning.",
    "There are no secrets to success. It is the result of preparation, hard work, and learning from failure.",
    "The price of success is hard work, dedication to the job at hand, and the determination that whether we win or lose, we have applied the best of ourselves to the task at hand.",
    "Perseverance is the hard work you do after you get tired of doing the hard work you already did.",
    "Luck? I don't know anything about luck. I've never banked on it and I'm afraid of people who do. Luck to me is something else: Hard work - and realizing what is opportunity and what isn't.",
  ];

  useEffect(() => {
    let random = Math.floor(Math.random() * 10);
    setQuote(quotes[random]);
  }, []);

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <Grid alignItems="center" container justify="space-between" spacing={3}>
        <Grid item md={6} xs={12}>
          <Typography component="h1" gutterBottom variant="h5">
            <b>{moment().format("LLLL")}</b>
          </Typography>
          <Typography gutterBottom variant="subtitle1">
            {quote}
          </Typography>
        </Grid>
        <Hidden smDown>
          <Grid item md={6}>
            <img alt="Cover" className={classes.image} src={Image} />
          </Grid>
        </Hidden>
      </Grid>
    </div>
  );
};

export default Header;
