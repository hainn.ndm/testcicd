import React, { ComponentType, useState, useEffect } from "react";
import { useDispatch } from "react-redux";

import { closeModal, openModal } from "store/redux/actions/GlobalModalAction";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Theme } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    paddingBottom: "50px"
  },
  card: {
    backgroundColor: "#fff",
    minHeight: "15px",
  },
  cardHeader: {
    padding: `${theme.spacing(3)}px ${theme.spacing(1)}px ${theme.spacing(1)}px ${theme.spacing(1)}px`,
    backgroundColor: "#fff",
  },
  cardContent: {
    padding: `0px ${theme.spacing(1)}px`,
    paddingBottom: `0 !important`,
  },
  required: {
    color: "red",
  },
  choiceSex: {
    display: "flex",
    paddingTop: theme.spacing(1)
  },
  itemContent: {
    position: "relative",
  },
  btnRemove: {
    position: "absolute",
    height: 50,
    color: "#fff",
    fontSize: 14,
    fontWeight: 900,
    boxShadow: "none",
    margin: "15px 0 0 5px",
    right: "24px",
    top: "0px",
  },
  btnAdd: {
    margin: "15px 0 0 8px",
  }
}));

interface IProps {
  onChangeActivity: (activity: any) => void;
  activity: any;
}
interface Activity {
  starting: Date;
  ending: Date;
  unit: String;
  position: String;
  description: String;
}
interface Reference {
  name: String;
  position: String;
  company: String;
  phone: String;
  email: String;
}

const TabActivity: ComponentType<IProps> = (props: IProps) => {
  const classes = useStyles();
  const { onChangeActivity, activity } = props;
  const dispatch = useDispatch();

  const [activities, setActivities] = useState<Activity[]>([]);
  const [references, setReferences] = useState<Reference[]>([]);

  useEffect(() => {
    if (activity) {
      setActivities(activity.activities)
      setReferences(activity.references)
    }
  }, [activity])

  const getCurrentActivity = () => {
    const activity: any = {
      activities: activities,
      references: references
    }

    return activity
  }

  // handle activity
  const handleAddActivity = () => {
    const temp: Activity[] = [...activities]
    temp.push({
      starting: new Date("2015-12-20"),
      ending: new Date("2015-12-20"),
      unit: "",
      position: "",
      description: "",
    })

    setActivities(temp)
  }
  const handleRemoveItemActivity = (index: number = 0) => {
    const temp: Activity[] = [...activities];
    temp.splice(index, 1);

    setActivities(temp);
    onChangeActivity({
      activities: temp,
      references: references
    })
  }
  const changeActivityTab = (index: number = 0, activity: Activity) => {
    const temp = [...activities];
    temp[index] = { ...activity }

    setActivities(temp)
  }

  // handle reference
  const handleAddReference = () => {
    const temp: Reference[] = [...references]
    temp.push({
      name: "",
      position: "",
      company: "",
      phone: "",
      email: ""
    })

    setReferences(temp)
  }
  const handleRemoveItemReference = (index: number = 0) => {
    const temp: Reference[] = [...references];
    temp.splice(index, 1);

    setReferences(temp);
    onChangeActivity({
      activities: activities,
      references: temp
    })
  }
  const changeReferenceTab = (index: number = 0, reference: Reference) => {
    const temp: Reference[] = [...references];
    temp[index] = { ...reference }

    setReferences(temp)
  }

  const handleRemoveItem = (index: number = 0, item: string = "") => {
    let payload = {
      title: "Xác nhận hành động?",
      content: (
        <>
          Bạn chắc chắn muốn xóa chứ? Nhấn <strong>OK</strong> để tiếp
          tục
        </>
      ),
      action: () => {
        switch (item) {
          case "ACTIVITY":
            handleRemoveItemActivity(index)
            break;
          case "REFERENCE":
            handleRemoveItemReference(index)
            break;
          default:
            break;
        }
        dispatch(closeModal());
      },
    };
    dispatch(openModal(payload));
  }

  return (
    <Grid className={classes.root} container xs={12}>
      {/* // Handle Activity */}
      <Grid container xs={12} className={classes.card}>
        <Grid xs={12}>
          <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Hoạt động ngoài 
              </>
            }
          />
        </Grid>
        {
          activities.map((activity, index) => (
            <Grid key={index} container className={classes.itemContent} xs={12}>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Tên tổ chức  
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Tên tổ chức</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={activity.unit || ""}
                    onBlur={() => onChangeActivity(getCurrentActivity())}
                    onChange={(e: any) => changeActivityTab(index, { 
                      starting: activity.starting,
                      ending: activity.ending,
                      unit: e.target.value,
                      description: activity.description,
                      position: activity.position })}
                    label={"Tên tổ chức"}
                  />
                </CardContent>
              </Grid>
              <Grid container xs={12} sm={6} className={classes.card}>
                <Grid item xs={12} sm={6}>
                  <CardHeader
                      className={classes.cardHeader}
                      title={
                        <>
                          Ngày bắt đầu 
                        </>
                      }
                    />
                  <CardContent className={classes.cardContent}>
                    <Alert severity={"info"}>Ngày bắt đầu</Alert>
                    <TextField
                      id="date"
                      label="Ngày bắt đầu"
                      type="date"
                      value={activity.starting}
                      defaultValue={activity.starting}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e: any) => {
                        const newActivity: Activity = { 
                          starting: e.target.value,
                          ending: activity.ending,
                          unit: activity.unit,
                          description: activity.description,
                          position: activity.position }
                        changeActivityTab(index, newActivity);

                        const temp = [...activities];
                        temp[index] = { ...newActivity }                    
                        onChangeActivity({
                          activities: temp,
                          references: references
                        })
                      }}
                    />
                  </CardContent>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <CardHeader
                      className={classes.cardHeader}
                      title={
                        <>
                          Ngày kết thúc 
                        </>
                      }
                    />
                  <CardContent className={classes.cardContent}>
                    <Alert severity={"info"}>Ngày kết thúc</Alert>
                    <TextField
                      id="date"
                      label="Ngày kết thúc"
                      type="date"
                      value={activity.ending}
                      defaultValue={activity.ending}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e: any) => {
                        const newActivity: Activity = { 
                          starting: activity.starting,
                          ending: e.target.value,
                          unit: activity.unit,
                          description: activity.description,
                          position: activity.position }

                        changeActivityTab(index, newActivity);

                        const temp = [...activities];
                        temp[index] = { ...newActivity }                    
                        onChangeActivity({
                          activities: temp,
                          references: references
                        })
                      }}
                    />
                  </CardContent>
                </Grid>
              </Grid>
              <Grid item xs={12} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Vị trí công việc 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Vị trí công việc</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={activity.position}
                    onBlur={() => onChangeActivity(getCurrentActivity())}
                    onChange={(e: any) => changeActivityTab(index, { 
                      starting: activity.starting,
                      ending: activity.ending,
                      unit: activity.unit,
                      description: activity.description,
                      position: e.target.value })}
                    label={"Vị trí công việc"}
                  />
                </CardContent>
              </Grid>
              <Grid xs={12} className={classes.card}>
                <CardHeader
                  className={classes.cardHeader}
                  title={
                    <>
                      Mô tả chi tiết công việc 
                    </>
                  }
                />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Mô tả chi tiết công việc</Alert>
                  <TextField
                    multiline
                    rows={6}
                    value={activity.description}
                    label="Mô tả chi tiết công việc"
                    variant="outlined"
                    fullWidth
                    onBlur={() => onChangeActivity(getCurrentActivity())}
                    onChange={(e: any) => changeActivityTab(index, { 
                      starting: activity.starting,
                      ending: activity.ending,
                      unit: activity.unit,
                      description: e.target.value,
                      position: activity.position })}
                  />
                </CardContent>
                <div className={classes.btnRemove}>
                  <Button color="primary" onClick={ () => handleRemoveItem(index, "ACTIVITY") } variant="contained">
                    Xóa
                  </Button>
                </div>
              </Grid>
            </Grid>
          ))
        }
        <Grid xs={12}>
          <Button className={classes.btnAdd} variant="contained" color="primary" onClick={handleAddActivity}>
            Thêm hoạt động khác
          </Button>
        </Grid>
      </Grid>
      {/* // Handle Reference */}
      <Grid container xs={12} className={classes.card}>
        <Grid xs={12}>
          <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Người tham chiếu 
              </>
            }
          />
        </Grid>
        {
          references.map((reference, index) => (
            <Grid key={index} container className={classes.itemContent} xs={12}>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Tên người tham chiếu 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Tên người tham chiếu</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={reference.name}
                    onBlur={() => onChangeActivity(getCurrentActivity())}
                    onChange={(e: any) => changeReferenceTab(index, { 
                      name: e.target.value,
                      position: reference.position,
                      company: reference.company,
                      phone: reference.phone,
                      email: reference.email })}
                    label={"Tên người tham chiếu"}
                  />
                </CardContent>
              </Grid>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Vị trí công việc 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Vị trí công việc</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={reference.position}
                    onBlur={() => onChangeActivity(getCurrentActivity())}
                    onChange={(e: any) => changeReferenceTab(index, { 
                      name: reference.name,
                      position: e.target.value,
                      company: reference.company,
                      phone: reference.phone,
                      email: reference.email })}
                    label={"Vị trí làm việc"}
                  />
                </CardContent>
              </Grid>
              <Grid item xs={12} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Tên công ty 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Tên công ty</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={reference.company}
                    onBlur={() => onChangeActivity(getCurrentActivity())}
                    onChange={(e: any) => changeReferenceTab(index, { 
                      name: reference.name,
                      position: reference.position,
                      company: e.target.value,
                      phone: reference.phone,
                      email: reference.email })}
                    label={"Tên công ty"}
                  />
                </CardContent>
              </Grid>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Số điện thoại 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Số điện thoại liên hệ</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={reference.phone}
                    onBlur={() => onChangeActivity(getCurrentActivity())}
                    onChange={(e: any) => changeReferenceTab(index, { 
                      name: reference.name,
                      position: reference.position,
                      company: reference.company,
                      phone: e.target.value,
                      email: reference.email })}
                    label={"Số điện thoại"}
                  />
                </CardContent>
              </Grid>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Địa chỉ email 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Địa chỉ email liên hệ</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={reference.email}
                    onBlur={() => onChangeActivity(getCurrentActivity())}
                    onChange={(e: any) => changeReferenceTab(index, { 
                      name: reference.name,
                      position: reference.position,
                      company: reference.company,
                      phone: reference.phone,
                      email: e.target.value })}
                    label={"Địa chỉ email"}
                  />
                </CardContent>
                <div className={classes.btnRemove}>
                  <Button color="primary" onClick={ () => handleRemoveItem(index, "REFERENCE") } variant="contained">
                    Xóa
                  </Button>
                </div>
              </Grid>
            </Grid>
          ))
        }
        <Grid xs={12}>
          <Button className={classes.btnAdd} variant="contained" color="primary" onClick={handleAddReference}>
            Thêm người tham chiếu
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default TabActivity;
