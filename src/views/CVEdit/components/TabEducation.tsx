import React, { ComponentType, useState, useEffect } from "react";
import { useDispatch } from "react-redux";

import makeStyles from "@material-ui/core/styles/makeStyles";
import { Theme } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { closeModal, openModal } from "store/redux/actions/GlobalModalAction";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    paddingBottom: "50px"
  },
  card: {
    backgroundColor: "#fff",
    minHeight: "15px",
  },
  cardHeader: {
    padding: `${theme.spacing(3)}px ${theme.spacing(1)}px ${theme.spacing(1)}px ${theme.spacing(1)}px`,
    backgroundColor: "#fff",
  },
  cardContent: {
    padding: `0px ${theme.spacing(1)}px`,
    paddingBottom: `0 !important`,
  },
  required: {
    color: "red",
  },
  choiceSex: {
    display: "flex",
    paddingTop: theme.spacing(1)
  },
  itemContent: {
    position: "relative",
  },
  btnRemove: {
    position: "absolute",
    height: 50,
    color: "#fff",
    fontSize: 14,
    fontWeight: 900,
    boxShadow: "none",
    margin: "15px 0 0 5px",
    right: "24px",
    top: "0px",
  },
  btnAdd: {
    margin: "15px 0 0 8px",
  }
}));

interface IProps {
  onChangeEducation: (education: any) => void;
  education: any;
}
interface Award {
  awardName: String;
  dateOfReceiveAward: Date;
  awardDescription: String;
}
interface Certificate {
  certificateName: String;
  dateOfReceiveCertificate: Date;
  certificateDescription: String;
}
interface Education {
  starting: Date;
  ending: Date;
  specialize: String;
  description: String;
  unit: String;
}

const TabEducation: ComponentType<IProps> = (props: IProps) => {
  const classes = useStyles();
  const { onChangeEducation, education } = props;
  const dispatch = useDispatch();

  const [awards, setAwards] = useState<Award[]>([]);
  const [certificates, setCertificates] = useState<Certificate[]>([]);
  const [educations, setEducations] = useState<Education[]>([]);

  useEffect(() => {
    if (education) {
      setEducations(education.educations || [])
      setCertificates(education.certificates || [])
      setAwards(education.awards || [])
    }
  }, [education])

  const getCurrentEducation = () => {
    const edu: any = {
      educations: educations,
      awards: awards,
      certificates: certificates,
    }

    return edu
  }

  // handle award
  const handleAddAward = () => {
    const temp: Award[] = [...awards]
    temp.push({
      awardName: "",
      dateOfReceiveAward: new Date("2015-12-20"),
      awardDescription: ""
    })

    setAwards(temp)
  }
  const handleRemoveItemAward = (index: number = 0) => {
    const temp: Award[] = [...awards]
    temp.splice(index, 1)

    setAwards(temp)
    onChangeEducation({
      educations: educations,
      awards: temp,
      certificates: certificates,
    })
  }
  const changeAwardTab = (index: number = 0, award: Award) => {
    const temp = [...awards]
    temp[index] = { ...award }

    setAwards(temp)
  }

  // handle certificate
  const handleAddCertificate = () => {
    const temp: Certificate[] = [...certificates]
    temp.push({
      certificateName: "",
      dateOfReceiveCertificate: new Date("2015-12-20"),
      certificateDescription: ""
    })

    setCertificates(temp)
  }
  const handleRemoveItemCertificate = (index: number = 0) => {
    const temp: Certificate[] = [...certificates];
    temp.splice(index, 1);

    setCertificates(temp);
    onChangeEducation({
      educations: educations,
      awards: awards,
      certificates: temp,
    })
  }
  const changeCertificateTab = (index: number = 0, certificate: Certificate) => {
    const temp = [...certificates];
    temp[index] = { ...certificate }

    setCertificates(temp)
  }

  // handle education
  const handleAddEducation = () => {
    const temp: Education[] = [...educations]
    temp.push({
      starting: new Date("2015-12-20"),
      ending: new Date("2015-12-20"),
      specialize: "",
      description: "",
      unit: ""
    })

    setEducations(temp)
  }
  const handleRemoveItemEducation = (index: number = 0) => {
    const temp: Education[] = [...educations]
    temp.splice(index, 1)

    setEducations(temp)
    onChangeEducation({
      awards: awards,
      educations: temp,
      certificates: certificates,
    })
  }
  const changeEducationTab = (index: number = 0, education: Education) => {
    const temp: Education[] = [...educations]
    temp[index] = { ...education }

    setEducations(temp)
  }

  const handleRemoveItem = (index: number = 0, item: string = "") => {
    let payload = {
      title: "Xác nhận hành động?",
      content: (
        <>
          Bạn chắc chắn muốn xóa chứ? Nhấn <strong>OK</strong> để tiếp
          tục
        </>
      ),
      action: () => {
        switch (item) {
          case "CERTIFICATE":
            handleRemoveItemCertificate(index)
            break;
          case "AWARD":
            handleRemoveItemAward(index)
            break;
          case "EDUCATION":
            handleRemoveItemEducation(index)
            break;
          default:
            break;
        }
        dispatch(closeModal());
      },
    };
    dispatch(openModal(payload));
  }

  return (
    <Grid className={classes.root} container xs={12}>
      <Grid container xs={12} className={classes.card}>
        <Grid xs={12}>
          <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Học vấn 
              </>
            }
          />
        </Grid>
        {
          educations.map((education, index) => (
            <Grid key={index} container className={classes.itemContent} xs={12}>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Đơn vị học tập  
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Đơn vị ứng viên học tập</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={education.unit}
                    onBlur={() => onChangeEducation(getCurrentEducation())}
                    onChange={(e: any) => changeEducationTab(index, { 
                      starting: education.starting,
                      ending: education.ending,
                      specialize: education.specialize,
                      description: education.description,
                      unit: e.target.value })}
                    label={"Đơn vị học tập"}
                  />
                </CardContent>
              </Grid>
              <Grid container xs={12} sm={6} className={classes.card}>
                <Grid item xs={12} sm={6}>
                  <CardHeader
                      className={classes.cardHeader}
                      title={
                        <>
                          Ngày nhập học 
                        </>
                      }
                    />
                  <CardContent className={classes.cardContent}>
                    <Alert severity={"info"}>Ngày ứng viên nhập học</Alert>
                    <TextField
                      id="date"
                      label="Ngày nhập học"
                      type="date"
                      value={education.starting}
                      defaultValue={education.starting}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e: any) => {
                        const newEducation: Education = { 
                          starting: e.target.value,
                          ending: education.ending,
                          specialize: education.specialize,
                          description: education.description,
                          unit: education.unit }
                          changeEducationTab(index, newEducation)

                          const temp: Education[] = [ ...educations ]
                          temp[index] = { ...newEducation }
                          onChangeEducation({
                            educations: temp,
                            awards: awards,
                            certificates: certificates,
                          })
                        }
                      }
                    />
                  </CardContent>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <CardHeader
                      className={classes.cardHeader}
                      title={
                        <>
                          Ngày tốt nghiệp 
                        </>
                      }
                    />
                  <CardContent className={classes.cardContent}>
                    <Alert severity={"info"}>Ngày ứng viên tốt nghiệp</Alert>
                    <TextField
                      id="date"
                      label="Ngày tốt nghiệp"
                      type="date"
                      value={education.ending}
                      defaultValue={education.ending}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e: any) => {
                          const newEducation: Education = { 
                            starting: education.starting,
                            ending: e.target.value,
                            specialize: education.specialize,
                            description: education.description,
                            unit: education.unit }
                          changeEducationTab(index, newEducation)

                          const temp: Education[] = [ ...educations ]
                          temp[index] = { ...newEducation }
                          onChangeEducation({
                            educations: temp,
                            awards: awards,
                            certificates: certificates,
                          })
                        }
                      }
                    />
                  </CardContent>
                </Grid>
              </Grid>
              <Grid item xs={12} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Chuyên ngành học 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Chuyên ngành học tại đại học</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={education.specialize}
                    onBlur={() => onChangeEducation(getCurrentEducation())}
                    onChange={(e: any) => changeEducationTab(index, { 
                      starting: education.starting,
                      ending: education.ending,
                      specialize: e.target.value,
                      description: education.description,
                      unit: education.unit })}
                    label={"Chuyên ngành học"}
                  />
                </CardContent>
              </Grid>
              <Grid xs={12} className={classes.card}>
                <CardHeader
                  className={classes.cardHeader}
                  title={
                    <>
                      Mô tả chi tiết 
                    </>
                  }
                />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Mô tả chi tiết</Alert>
                  <TextField
                    multiline
                    rows={6}
                    value={education.description}
                    label="Mô tả chi tiết"
                    variant="outlined"
                    fullWidth
                    onBlur={() => onChangeEducation(getCurrentEducation())}
                    onChange={(e: any) => changeEducationTab(index, { 
                      starting: education.starting,
                      ending: education.ending,
                      specialize: education.specialize,
                      description: e.target.value,
                      unit: education.unit })}
                  />
                </CardContent>
                <div className={classes.btnRemove}>
                  <Button color="primary" onClick={ () => handleRemoveItem(index, "EDUCATION") } variant="contained">
                    Xóa
                  </Button>
                </div>
              </Grid>
            </Grid>
          ))
        }
        <Grid xs={12}>
          <Button className={classes.btnAdd} variant="contained" color="primary" onClick={handleAddEducation}>
            Thêm học vấn
          </Button>
        </Grid>
      </Grid>
      <Grid container xs={12} className={classes.card}>
        <Grid xs={12}>
          <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Giải thưởng 
              </>
            }
          />
        </Grid>
        {
          awards.map((award, index) => (
            <Grid key={index} container className={classes.itemContent} xs={12}>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Tên giải thưởng  
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Tên giải thưởng</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={award.awardName || ""}
                    onBlur={() => onChangeEducation(getCurrentEducation())}
                    onChange={(e: any) => changeAwardTab(index, { awardName: e.target.value, awardDescription: award.awardDescription, dateOfReceiveAward: award.dateOfReceiveAward })}
                    label={"Tên giải thưởng"}
                  />
                </CardContent>
              </Grid>
              <Grid container xs={12} sm={6} className={classes.card}>
                <Grid item xs={12}>
                  <CardHeader
                      className={classes.cardHeader}
                      title={
                        <>
                          Ngày nhận giải thưởng 
                        </>
                      }
                    />
                  <CardContent className={classes.cardContent}>
                    <Alert severity={"info"}>Ngày nhận giải thưởng</Alert>
                    <TextField
                      id="date"
                      label="Ngày nhận giải thưởng"
                      type="date"
                      value={award.dateOfReceiveAward}
                      defaultValue={award.dateOfReceiveAward}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e: any) => {
                          const newAward: Award = { awardName: award.awardName, awardDescription: award.awardDescription, dateOfReceiveAward: e.target.value }
                          changeAwardTab(index, newAward)

                          const temp: Award[] = [...awards]
                          temp[index] = {...newAward}
                          onChangeEducation({
                            educations: educations,
                            awards: temp,
                            certificates: certificates,
                          })
                        }
                      }
                    />
                  </CardContent>
                </Grid>
              </Grid>
              <Grid xs={12} className={classes.card}>
                <CardHeader
                  className={classes.cardHeader}
                  title={
                    <>
                      Mô tả chi tiết giải thưởng 
                    </>
                  }
                />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Mô tả chi tiết giải thưởng</Alert>
                  <TextField
                    multiline
                    rows={6}
                    value={award.awardDescription}
                    label="Mô tả chi tiết giải thưởng"
                    variant="outlined"
                    fullWidth
                    onBlur={() => onChangeEducation(getCurrentEducation())}
                    onChange={(e) => changeAwardTab(index, { awardName: award.awardName, awardDescription: e.target.value, dateOfReceiveAward: award.dateOfReceiveAward }) }
                  />
                </CardContent>
                <div className={classes.btnRemove}>
                  <Button color="primary" onClick={ () => handleRemoveItem(index, "AWARD") } variant="contained">
                    Xóa
                  </Button>
                </div>
              </Grid>
            </Grid>
          ))
        }
        <Grid xs={12}>
          <Button className={classes.btnAdd} variant="contained" color="primary" onClick={handleAddAward}>
            Thêm giải thưởng
          </Button>
        </Grid>
      </Grid>
      <Grid container xs={12} className={classes.card}>
        <Grid xs={12}>
          <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Chứng chỉ 
              </>
            }
          />
        </Grid>
        {
          certificates.map((certificate, index) => (
            <Grid key={index} container className={classes.itemContent} xs={12}>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Tên chứng chỉ  
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Tên chứng chỉ</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={certificate.certificateName || ""}
                    onBlur={() => onChangeEducation(getCurrentEducation())}
                    onChange={(e: any) => changeCertificateTab(index, { certificateName: e.target.value, certificateDescription: certificate.certificateDescription, dateOfReceiveCertificate: certificate.dateOfReceiveCertificate })}
                    label={"Tên chứng chỉ"}
                  />
                </CardContent>
              </Grid>
              <Grid container xs={12} sm={6} className={classes.card}>
                <Grid item xs={12}>
                  <CardHeader
                      className={classes.cardHeader}
                      title={
                        <>
                          Ngày nhận chứng chỉ 
                        </>
                      }
                    />
                  <CardContent className={classes.cardContent}>
                    <Alert severity={"info"}>Ngày nhận chứng chỉ</Alert>
                    <TextField
                      id="date"
                      label="Ngày nhận chứng chỉ"
                      type="date"
                      value={certificate.dateOfReceiveCertificate}
                      defaultValue={certificate.dateOfReceiveCertificate}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e: any) => {
                          const newCertificate: Certificate = { certificateName: certificate.certificateName, certificateDescription: certificate.certificateDescription, dateOfReceiveCertificate: e.target.value }
                          changeCertificateTab(index, newCertificate)

                          const temp: Certificate[] = [...certificates]
                          temp[index] = {...newCertificate}
                          onChangeEducation({
                            educations: educations,
                            awards: awards,
                            certificates: temp,
                          })
                        }
                      }
                    />
                  </CardContent>
                </Grid>
              </Grid>
              <Grid xs={12} className={classes.card}>
                <CardHeader
                  className={classes.cardHeader}
                  title={
                    <>
                      Mô tả chi tiết chứng chỉ 
                    </>
                  }
                />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Mô tả chi tiết chứng chỉ</Alert>
                  <TextField
                    multiline
                    rows={6}
                    value={certificate.certificateDescription}
                    label="Mô tả chi tiết chứng chỉ"
                    variant="outlined"
                    fullWidth
                    onBlur={() => onChangeEducation(getCurrentEducation())}
                    onChange={(e) => changeCertificateTab(index, { certificateName: certificate.certificateName, certificateDescription: e.target.value, dateOfReceiveCertificate: certificate.dateOfReceiveCertificate }) }
                  />
                </CardContent>
                <div className={classes.btnRemove}>
                  <Button color="primary" onClick={ () => handleRemoveItem(index, "CERTIFICATE") } variant="contained">
                    Xóa
                  </Button>
                </div>
              </Grid>
            </Grid>
          ))
        }
        <Grid xs={12}>
          <Button className={classes.btnAdd} variant="contained" color="primary" onClick={handleAddCertificate}>
            Thêm chứng chỉ
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default TabEducation;
