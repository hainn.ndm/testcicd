import React, { ComponentType, useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import Page from "components/Page/Page";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Theme } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TabInformation from 'views/CVEdit/components/TabInformation';
import TabEducation from 'views/CVEdit/components/TabEducation';
import TabExperience from 'views/CVEdit/components/TabExperience';
import TabActivity from 'views/CVEdit/components/TabActivity';
import { closeModal, openModal } from "store/redux/actions/GlobalModalAction";
import { axios } from "utils/axiosInstance";
import { API } from "utils/api";
import {apolloClient} from "utils/apolloClient";
import {SINGLE_CITY} from "graphql/city/query";
import {SINGLE_DISTRICT} from "graphql/district/query";
import {SINGLE_WARD} from "graphql/ward/query";
import {timestampDateFormat2} from "helpers/date";
import { enqueueSnackbar } from "store/redux/actions/GlobalToastAction";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    overflow: "hidden",
    padding: theme.spacing(3),
  },
  container: {
    marginTop: theme.spacing(4),
    overflow: "hidden"
  },
  tabContent: {
    width: "100%",
  },
  btnSubmit: {
    float: "right",
    height: 50,
    color: "#fff",
    fontSize: 14,
    fontWeight: 900,
    boxShadow: "none",
    width: "100%",
    margin: "15px 0 0 5px",
    "&:hover": {
      boxShadow: "none",
    }
  },
}));

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      style={{ width: "100%" }}
      role="tabpanel"
      hidden={value !== index}
      id={`wrapped-tabpanel-${index}`}
      aria-labelledby={`wrapped-tab-${index}`}
      {...other}
    >
      {value === index && (
        children
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `wrapped-tab-${index}`,
    'aria-controls': `wrapped-tabpanel-${index}`,
  };
}

interface IProps {}

const Edit: ComponentType<IProps> = (props: IProps) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const params = useParams();

  const [value, setValue] = useState('info');
  const [info, setInfo] = useState<any>();
  const [education, setEducation] = useState<any>();
  const [experience, setExperience] = useState<any>();
  const [activity, setActivity] = useState<any>();

  useEffect(() => {
    // @ts-ignore
    axios.get(`${API.GET_CV_BY_ID}/${params.slug}`).then(async (r) => {
      if (r.status === 200) {
        let address: any = null
        if (r.data && r.data.tinh && r.data.huyen && r.data.xa) {
          address = await Promise.all([apolloClient.query({query: SINGLE_CITY, variables: { _id: r.data.tinh }}),
            apolloClient.query({query: SINGLE_DISTRICT, variables: { _id: r.data.huyen }}),
            apolloClient.query({query: SINGLE_WARD, variables: { _id: r.data.xa }})])

            setInfo({
              city: (address && address[0])? {node: address[0].data.city} : {node: ""},
              district: (address && address[1])? {node: address[1].data.district} : {node: ""},
              ward: (address && address[2])? {node: address[2].data.ward} : {node: ""},
              specificAddress: r.data.dia_chi_cu_the,
              sex: r.data.gioi_tinh || "",
              phone: r.data.so_dien_thoai || "",
              birthday: timestampDateFormat2(new Date(r.data.ngay_sinh).getTime()),
              email: r.data.email || "",
              name: r.data.ho_va_ten || "",
              facebook: r.data.facebook || "",
              linkedin: r.data.linkedin || "",
              website: r.data.website || "",
              infoMore: r.data.thong_tin_thems[0]? r.data.thong_tin_thems[0].thong_tin_them : "",
              preference: r.data.so_thiches[0]? r.data.so_thiches[0].so_thich : "",
              file: r.data.file || "",
            })
        } else {
          setInfo({
            specificAddress: r.data.dia_chi_cu_the,
            sex: r.data.gioi_tinh || "",
            phone: r.data.so_dien_thoai || "",
            birthday: timestampDateFormat2(new Date(r.data.ngay_sinh).getTime()),
            email: r.data.email || "",
            name: r.data.ho_va_ten || "",
            facebook: r.data.facebook || "",
            linkedin: r.data.linkedin || "",
            website: r.data.website || "",
            infoMore: r.data.thong_tin_thems[0]? r.data.thong_tin_thems[0].thong_tin_them : "",
            preference: r.data.so_thiches[0]? r.data.so_thiches[0].so_thich : "",
            file: r.data.file || "",
          });
        }

        setEducation({
          educations: r.data.hoc_vans? r.data.hoc_vans.map((element: any) => ({
            starting: timestampDateFormat2(new Date(element.bat_dau).getTime()),
            ending: timestampDateFormat2(new Date(element.ket_thuc).getTime()),
            unit: element.don_vi,
            specialize: element.chuyen_nganh,
            description: element.mo_ta
          })) : [],
          awards: r.data.giai_thuongs? r.data.giai_thuongs.map((element: any) => ({
            awardName: element.ten_giai_thuong,
            dateOfReceiveAward: timestampDateFormat2(new Date(element.ngay_nhan_giai).getTime()),
            awardDescription: element.mo_ta_giai_thuong,
          })) : [],
          certificates: r.data.chung_chis? r.data.chung_chis.map((element: any) => ({
            certificateName: element.ten_chung_chi,
            dateOfReceiveCertificate: timestampDateFormat2(new Date(element.nam).getTime()),
            certificateDescription: element.mo_ta_chung_chi,
          })) : [],
        })
        setExperience({
          experiences: r.data.kinh_nghiem_lam_viecs? r.data.kinh_nghiem_lam_viecs.map((element: any) => ({
            starting: timestampDateFormat2(new Date(element.bat_dau).getTime()),
            ending: timestampDateFormat2(new Date(element.ket_thuc).getTime()),
            unit: element.to_chuc,
            position: element.vi_tri_cong_viec,
            level: element.cap_bac,
            description: element.mo_ta_cong_viec
          })) : [],
          projects: r.data.du_ans? r.data.du_ans.map((element: any) => ({
            starting: timestampDateFormat2(new Date(element.bat_dau).getTime()),
            ending: timestampDateFormat2(new Date(element.ket_thuc).getTime()),
            position: element.vi_tri_tham_gia,
            name: element.ten_du_an,
            description: element.mo_ta_chi_tiet
          })) : [],
          skills: r.data.ky_nangs? r.data.ky_nangs.map((element: any) => ({
            point: element.thang_diem,
            name: element.ten_ky_nang,
            description: element.mo_ta
          })) : [],
        })
        setActivity({
          references: r.data.nguoi_tham_chieus? r.data.nguoi_tham_chieus.map((element: any) => ({
            name: element.ten_to_chuc,
            company: element.cong_ty,
            position: element.vi_tri_cong_viec,
            phone: element.dien_thoai,
            email: element.email
          })) : [],
          activities: r.data.hoat_dongs? r.data.hoat_dongs.map((element: any) => ({
            starting: timestampDateFormat2(new Date(element.bat_dau).getTime()),
            ending: timestampDateFormat2(new Date(element.ket_thuc).getTime()),
            unit: element.ten_to_chuc,
            position: element.vi_tri_tham_gia,
            description: element.mo_ta_chi_tiet
          })) : [],
        })
      }
    }).catch((err) => alert(err.response.data.message.join("\n")));
  }, []);

  const isInputDone = () => {
    if (
      info && info.name && info.email && info.birthday && info.phone
    ) {
      return true;
    }
    return false;
  };

  const handleChange = (event: any, newValue: string) => {
    setValue(newValue);
  };

  const changeInfo = (info: any) => {
    setInfo(info)
  }

  const changeEducation = (experience: any) => {
    setEducation(experience)
  }

  const changeExperience = (education: any) => {
    setExperience(education)
  }

  const changeActivity = (activity: any) => {
    setActivity(activity)
  }

  const handleSave = () => {
    const CVPayload = {
      email: info.email || "",
      ho_va_ten: info.name || "",
      gioi_tinh: info.sex || "nam",
      ngay_sinh: info.birthday || new Date(),
      so_dien_thoai: info.phone || "",
      tinh: info.city? info.city.node._id : "",
      huyen: info.district? info.district.node._id : "",
      xa: info.ward? info.ward.node._id : "",
      dia_chi_cu_the: info.specificAddress || "",
      facebook: info.facebook || "",
      linkedin: info.linkedin || "",
      website: info.website || "",
      file: info.file || "",
      thong_tin_thems: info.infoMore? [{
        thong_tin_them: info.infoMore
      }] : [],
      so_thiches: info.preference? [{
        so_thich: info.preference
      }] : [],
      hoc_vans: education.educations.map((element: any) => ({
        bat_dau: element.starting,
        ket_thuc: element.ending,
        don_vi: element.unit,
        chuyen_nganh: element.specialize,
        mo_ta: element.description,
      })),
      giai_thuongs: education.awards.map((element: any) => ({
        ngay_nhan_giai: element.dateOfReceiveAward,
        ten_giai_thuong: element.awardName,
        mo_ta_giai_thuong: element.awardDescription,
      })),
      chung_chis: education.certificates.map((element: any) => ({
        nam: element.dateOfReceiveCertificate,
        ten_chung_chi: element.certificateName,
        mo_ta_chung_chi: element.certificateDescription,
      })),
      kinh_nghiem_lam_viecs: experience.experiences.map((element: any) => ({
        bat_dau: element.starting,
        ket_thuc: element.ending,
        to_chuc: element.unit,
        vi_tri_cong_viec: element.position,
        cap_bac: element.level,
        mo_ta_cong_viec: element.description,
      })),
      du_ans: experience.projects.map((element: any) => ({
        bat_dau: element.starting,
        ket_thuc: element.ending,
        ten_du_an: element.name,
        vi_tri_tham_gia: element.position,
        mo_ta_chi_tiet: element.description,
      })),
      ky_nangs: experience.skills.map((element: any) => ({
        thang_diem: element.point,
        ten_ky_nang: element.name,
        mo_ta: element.description,
      })),
      hoat_dongs: activity.activities.map((element: any) => ({
        bat_dau: element.starting,
        ket_thuc: element.ending,
        ten_to_chuc: element.unit,
        vi_tri_tham_gia: element.position,
        mo_ta_chi_tiet: element.description,
      })),
      nguoi_tham_chieus: activity.references.map((element: any) => ({
        ten_nguoi_tham_chieu: element.name,
        vi_tri_cong_viec: element.position,
        cong_ty: element.company,
        dien_thoai: element.phone,
        email: element.email,
      })),
    }

    if (!isInputDone()) {
      const toast = {
        message: "Cần điền đầy đủ các mục có dấu *",
        options: {
          key: new Date().getTime() + Math.random(),
          variant: "error",
        },
      };

      dispatch(enqueueSnackbar(toast));
      dispatch(closeModal());
      return
    }

    // @ts-ignore
    axios.put(`${API.UPDATE_CV_BY_ID}/${params.slug}`, {
      ...CVPayload,
    }, {
      withCredentials: true
    }).then((r) => {
      if (r.status === 200) {
        window.location.href = `/cv/list`;
      }
    }).catch((err) => {
      alert(err.response.data.message.join("\n"))
    });
    dispatch(closeModal());
  }

  const handleSubmit = () => {
    let payload = {
      title: "Xác nhận hành động?",
      content: (
        <>
          Bạn chắc chắn tạo CV với những thông tin trên? Nhấn <strong>OK</strong> để tiếp
          tục
        </>
      ),
      action: () => handleSave(),
    };
    dispatch(openModal(payload));
  }

  return (
    <Page
      title={"Chỉnh sửa dữ liệu CV"}
      heading={ "Chỉnh sửa dữ liệu CV"}
      className={classes.root}
    >
      <Grid className={classes.container} container spacing={2}>
        <AppBar position="static">
          <Tabs value={value} onChange={handleChange} aria-label="wrapped label tabs example">
            <Tab value="info" label="Thông tin cơ bản" wrapped {...a11yProps('info')} />
            <Tab value="education" label="Học vấn" {...a11yProps('education')} />
            <Tab value="experience" label="Kinh nghiệm làm việc" {...a11yProps('experience')} />
            <Tab value="activity" label="Hoạt động khác" {...a11yProps('activity')} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index="info">
          <TabInformation info={info} onChangeInfo={changeInfo} />
        </TabPanel>
        <TabPanel value={value} index="education">
          <TabEducation education={education} onChangeEducation={changeEducation} />
        </TabPanel>
        <TabPanel value={value} index="experience">
          <TabExperience experience={experience} onChangeExperience={changeExperience} />
        </TabPanel>
        <TabPanel value={value} index="activity">
          <TabActivity activity={activity} onChangeActivity={changeActivity} />
        </TabPanel>
      </Grid>
      <div>
        <Button color="primary" variant="contained" onClick={handleSubmit} className={classes.btnSubmit}>
          Hoàn tất tải lên CV
        </Button>
      </div>
    </Page>
  );
};

export default Edit;
