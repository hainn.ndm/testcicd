import React, { ComponentType, useState } from "react";
import { useDispatch } from "react-redux";
import Page from "components/Page/Page";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Theme } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TabInformation from 'views/CVCreate/components/TabInformation';
import TabEducation from 'views/CVCreate/components/TabEducation';
import TabExperience from 'views/CVCreate/components/TabExperience';
import TabActivity from 'views/CVCreate/components/TabActivity';
import { axios } from "utils/axiosInstance";
import { API } from "utils/api";
import { closeModal, openModal } from "store/redux/actions/GlobalModalAction";
import { enqueueSnackbar } from "store/redux/actions/GlobalToastAction";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    overflow: "hidden",
    padding: theme.spacing(3),
  },
  container: {
    marginTop: theme.spacing(4),
    overflow: "hidden"
  },
  tabContent: {
    width: "100%",
  },
  btnSubmit: {
    float: "right",
    height: 50,
    color: "#fff",
    fontSize: 14,
    fontWeight: 900,
    boxShadow: "none",
    width: "100%",
    margin: "15px 0 0 5px",
    "&:hover": {
      boxShadow: "none",
    }
  },
}));

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      style={{ width: "100%" }}
      role="tabpanel"
      hidden={value !== index}
      id={`wrapped-tabpanel-${index}`}
      aria-labelledby={`wrapped-tab-${index}`}
      {...other}
    >
      {value === index && (
        children
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `wrapped-tab-${index}`,
    'aria-controls': `wrapped-tabpanel-${index}`,
  };
}

interface IProps {}

const CVCreate: ComponentType<IProps> = (props: IProps) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [value, setValue] = useState('info');
  const [info, setInfo] = useState<any>();
  const [education, setEducation] = useState<any>();
  const [experience, setExperience] = useState<any>();
  const [activity, setActivity] = useState<any>();

  const handleChange = (event: any, newValue: string) => {
    setValue(newValue);
  };

  const changeInfo = (info: any) => {
    setInfo(info)
  }

  const changeEducation = (experience: any) => {
    setEducation(experience)
  }

  const changeExperience = (education: any) => {
    setExperience(education)
  }

  const changeActivity = (activity: any) => {
    setActivity(activity)
  }

  const isInputDone = () => {
    if (
      info && info.name && info.email && info.birthday && info.phone
    ) {
      return true;
    }
    return false;
  };

  const handleSave = () => {
    let cachedCV: any = null
    // handle data cache
    if (window.localStorage) {
      cachedCV = {
        // @ts-ignore
        info: JSON.parse(window.localStorage.getItem("cvInformation")),
        // @ts-ignore
        education: JSON.parse(window.localStorage.getItem("cvEducation")),
        // @ts-ignore
        experience: JSON.parse(window.localStorage.getItem("cvExperience")),
        // @ts-ignore
        activity: JSON.parse(window.localStorage.getItem("cvActivity")),
      }
    }
    const CV = cachedCV || {
      info: info,
      education: education,
      experience: experience,
      activity: activity
    }

    const CVPayload = {
      email: CV.info.email,
      ho_va_ten: CV.info.name,
      gioi_tinh: CV.info.sex,
      ngay_sinh: CV.info.birthday,
      so_dien_thoai: CV.info.phone,
      tinh: CV.info.city? CV.info.city.node._id : "",
      huyen: CV.info.district? CV.info.district.node._id : "",
      xa: CV.info.ward? CV.info.ward.node._id : "",
      dia_chi_cu_the: CV.info.specificAddress || "N/A",
      facebook: CV.info.facebook || "",
      linkedin: CV.info.linkedin || "",
      website: CV.info.website || "",
      file: CV.info.file || ""
      ,
      thong_tin_thems: CV.info.infoMore? [{
        thong_tin_them: CV.info.infoMore || ""
      }] : [],
      so_thiches: CV.info.preference? [{
        so_thich: CV.info.preference || ""
      }] : [],
      hoc_vans: CV.education? CV.education.educations.map((element: any) => ({
        bat_dau: element.starting,
        ket_thuc: element.ending,
        don_vi: element.unit,
        chuyen_nganh: element.specialize,
        mo_ta: element.description,
      })) : [],
      giai_thuongs: CV.education? CV.education.awards.map((element: any) => ({
        ngay_nhan_giai: element.dateOfReceiveAward,
        ten_giai_thuong: element.awardName,
        mo_ta_giai_thuong: element.awardDescription,
      })) : [],
      chung_chis: CV.education? CV.education.certificates.map((element: any) => ({
        nam: element.dateOfReceiveCertificate,
        ten_chung_chi: element.certificateName,
        mo_ta_chung_chi: element.certificateDescription,
      })) : [],
      kinh_nghiem_lam_viecs: CV.experience? CV.experience.experiences.map((element: any) => ({
        bat_dau: element.starting,
        ket_thuc: element.ending,
        to_chuc: element.unit,
        vi_tri_cong_viec: element.position,
        cap_bac: element.level,
        mo_ta_cong_viec: element.description,
      })) : [],
      du_ans: CV.experience? CV.experience.projects.map((element: any) => ({
        bat_dau: element.starting,
        ket_thuc: element.ending,
        ten_du_an: element.name,
        vi_tri_tham_gia: element.position,
        mo_ta_chi_tiet: element.description,
      })) : [],
      ky_nangs: CV.experience? CV.experience.skills.map((element: any) => ({
        thang_diem: element.point,
        ten_ky_nang: element.name,
        mo_ta: element.description,
      })) : [],
      hoat_dongs: CV.activity? CV.activity.activities.map((element: any) => ({
        bat_dau: element.starting,
        ket_thuc: element.ending,
        ten_to_chuc: element.unit,
        vi_tri_tham_gia: element.position,
        mo_ta_chi_tiet: element.description,
      })) : [],
      nguoi_tham_chieus: CV.activity? CV.activity.references.map((element: any) => ({
        ten_nguoi_tham_chieu: element.name,
        vi_tri_cong_viec: element.position,
        cong_ty: element.company,
        dien_thoai: element.phone,
        email: element.email,
      })) : [],
    }

    if (!isInputDone()) {
      const toast = {
        message: "Cần điền đầy đủ các mục có dấu *",
        options: {
          key: new Date().getTime() + Math.random(),
          variant: "error",
        },
      };

      dispatch(enqueueSnackbar(toast));
      dispatch(closeModal());
      return
    }

    axios.post(`${API.CREATE_CV}`, {
      ...CVPayload,
    }, {
      withCredentials: true
    })
    .then((r) => {
      if (r.status === 201) {
        window.localStorage.removeItem("cvActivity")
        window.localStorage.removeItem("cvEducation")
        window.localStorage.removeItem("cvExperience")
        window.localStorage.removeItem("cvInformation")
        window.location.href = `/cv/list`;
      }
    })
    .catch((err) => {
      alert(err.response.data.message.join("\n"))
    });
    dispatch(closeModal());
  }

  const handleSubmit = () => {
    let payload = {
      title: "Xác nhận hành động?",
      content: (
        <>
          Bạn chắc chắn tạo CV với những thông tin trên? Nhấn <strong>OK</strong> để tiếp
          tục
        </>
      ),
      action: () => handleSave(),
    };
    dispatch(openModal(payload));
  }

  return (
    <Page
      title={"Tải lên dữ liệu CV"}
      heading={ "Tải lên dữ liệu CV"}
      className={classes.root}
    >
      <Grid className={classes.container} container spacing={2}>
        <AppBar position="static">
          <Tabs value={value} onChange={handleChange} aria-label="wrapped label tabs example">
            <Tab value="info" label="Thông tin cơ bản" wrapped {...a11yProps('info')} />
            <Tab value="education" label="Học vấn" {...a11yProps('education')} />
            <Tab value="experience" label="Kinh nghiệm làm việc" {...a11yProps('experience')} />
            <Tab value="activity" label="Hoạt động khác" {...a11yProps('activity')} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index="info">
          <TabInformation onChangeInfo={changeInfo} />
        </TabPanel>
        <TabPanel value={value} index="education">
          <TabEducation onChangeEducation={changeEducation} />
        </TabPanel>
        <TabPanel value={value} index="experience">
          <TabExperience onChangeExperience={changeExperience} />
        </TabPanel>
        <TabPanel value={value} index="activity">
          <TabActivity onChangeActivity={changeActivity} />
        </TabPanel>
      </Grid>
      <div>
        <Button color="primary" variant="contained" onClick={handleSubmit} className={classes.btnSubmit}>
          Hoàn tất tải lên CV
        </Button>
      </div>
    </Page>
  );
};

export default CVCreate;
