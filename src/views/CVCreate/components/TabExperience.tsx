import React, { ComponentType, useState, useEffect } from "react";
import { useDispatch } from "react-redux";

import { closeModal, openModal } from "store/redux/actions/GlobalModalAction";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Theme } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Slider from '@material-ui/core/Slider';



const useStyles = makeStyles((theme: Theme) => ({
  root: {
    paddingBottom: "50px"
  },
  card: {
    backgroundColor: "#fff",
    minHeight: "15px",
  },
  cardHeader: {
    padding: `${theme.spacing(3)}px ${theme.spacing(1)}px ${theme.spacing(1)}px ${theme.spacing(1)}px`,
    backgroundColor: "#fff",
  },
  cardContent: {
    padding: `0px ${theme.spacing(1)}px`,
    paddingBottom: `0 !important`,
  },
  required: {
    color: "red",
  },
  choiceSex: {
    display: "flex",
    paddingTop: theme.spacing(1)
  },
  itemContent: {
    position: "relative",
  },
  btnRemove: {
    position: "absolute",
    height: 50,
    color: "#fff",
    fontSize: 14,
    fontWeight: 900,
    boxShadow: "none",
    margin: "15px 0 0 5px",
    right: "24px",
    top: "0px",
  },
  btnAdd: {
    margin: "15px 0 0 8px",
  }
}));

interface IProps {
  onChangeExperience: (experience: any) => void;
}
interface Experience {
  starting: Date;
  ending: Date;
  unit: String;
  position: String;
  level: String;
  description: String;
}
interface Project {
  starting: Date;
  ending: Date;
  name: String;
  position: String;
  description: String;
}
interface Skill {
  name: String;
  point: number;
  description: String;
}


const TabExperience: ComponentType<IProps> = (props: IProps) => {
  const classes = useStyles();
  const { onChangeExperience } = props;
  const dispatch = useDispatch();

  const [experiences, setExperiences] = useState<Experience[]>([]);
  const [projects, setProjects] = useState<Project[]>([]);
  const [skills, setSkills] = useState<Skill[]>([]);

  useEffect(() => {
    // handle cache
    if (window.localStorage) {
      //@ts-ignore
      const cvEx: any = JSON.parse(window.localStorage.getItem("cvExperience"))
      if (cvEx) {
        setExperiences(cvEx.experiences || [])
        setProjects(cvEx.projects || [])
        setSkills(cvEx.skills || [])
      }
    }
  }, []);

  const getCurrentExperience = () => {
    const ex: any = {
      experiences: experiences,
      projects: projects,
      skills: skills,
    }

    if (window.localStorage) {
      window.localStorage.setItem("cvExperience", JSON.stringify(ex))
    }

    return ex
  }

  // handle experience
  const handleAddExperience = () => {
    const temp: Experience[] = [...experiences]
    temp.push({
      starting: new Date("2015-12-20"),
      ending: new Date("2015-12-20"),
      unit: "",
      position: "",
      level: "",
      description: "",
    })

    setExperiences(temp)
  }
  const handleRemoveItemExperience = (index: number = 0) => {
    const temp: Experience[] = [...experiences];
    temp.splice(index, 1);

    setExperiences(temp);
    onChangeExperience({
      experiences: temp,
      projects: projects,
      skills: skills,
    })
  }
  const changeExperienceTab = (index: number = 0, experience: Experience) => {
    const temp = [...experiences];
    temp[index] = { ...experience }

    setExperiences(temp)
  }

  // handle project
  const handleAddProject = () => {
    const temp: Project[] = [...projects]
    temp.push({
      starting: new Date("2015-12-20"),
      ending: new Date("2015-12-20"),
      name: "",
      position: "",
      description: "",
    })

    setProjects(temp)
  }
  const handleRemoveItemProject = (index: number = 0) => {
    const temp: Project[] = [...projects];
    temp.splice(index, 1);

    setProjects(temp);
    onChangeExperience({
      experiences: experiences,
      projects: temp,
      skills: skills,
    })
  }
  const onChangeProject = (index: number = 0, project: Project) => {
    const temp = [...projects];
    temp[index] = { ...project }

    setProjects(temp)
  }

  // handle skill
  const handleAddSkill = () => {
    const temp: Skill[] = [...skills]
    temp.push({
      name: "",
      point: 0,
      description: "",
    })

    setSkills(temp)
  }
  const handleRemoveItemSkill = (index: number = 0) => {
    const temp: Skill[] = [...skills];
    temp.splice(index, 1);

    setSkills(temp);
    onChangeExperience({
      experiences: experiences,
      projects: projects,
      skills: temp,
    })
  }
  const changeSkillTab = (index: number = 0, skill: Skill) => {
    const temp = [...skills];
    temp[index] = { ...skill }

    setSkills(temp)
  }

  const marksLevelSkill = [
    {
      value: 0,
      label: '0',
    },
    {
      value: 20,
      label: '20',
    },
    {
      value: 40,
      label: '40',
    },
    {
      value: 60,
      label: '60',
    },
    {
      value: 80,
      label: '80',
    },
    {
      value: 100,
      label: '100',
    },
  ];

  const handleRemoveItem = (index: number = 0, item: string = "") => {
    let payload = {
      title: "Xác nhận hành động?",
      content: (
        <>
          Bạn chắc chắn muốn xóa chứ? Nhấn <strong>OK</strong> để tiếp
          tục
        </>
      ),
      action: () => {
        switch (item) {
          case "EXPERIENCE":
            handleRemoveItemExperience(index)
            break;
          case "PROJECT":
            handleRemoveItemProject(index)
            break;
          case "SKILL":
            handleRemoveItemSkill(index)
            break;
          default:
            break;
        }
        dispatch(closeModal());
      },
    };
    dispatch(openModal(payload));
  }

  return (
    <Grid className={classes.root} container xs={12}>
      {/* // Handle Experience */}
      <Grid container xs={12} className={classes.card}>
        <Grid xs={12}>
          <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Kinh nghiệm làm việc 
              </>
            }
          />
        </Grid>
        {
          experiences.map((experience, index) => (
            <Grid key={index} container className={classes.itemContent} xs={12}>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Đơn vị làm việc  
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Đơn vị làm việc</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={experience.unit || ""}
                    onBlur={() => onChangeExperience(getCurrentExperience())}
                    onChange={(e: any) => changeExperienceTab(index, { 
                      starting: experience.starting,
                      ending: experience.ending,
                      unit: e.target.value,
                      description: experience.description,
                      level: experience.level,
                      position: experience.position })}
                    label={"Đơn vị làm việc"}
                  />
                </CardContent>
              </Grid>
              <Grid container xs={12} sm={6} className={classes.card}>
                <Grid item xs={12} sm={6}>
                  <CardHeader
                      className={classes.cardHeader}
                      title={
                        <>
                          Ngày bắt đầu 
                        </>
                      }
                    />
                  <CardContent className={classes.cardContent}>
                    <Alert severity={"info"}>Ngày bắt đầu</Alert>
                    <TextField
                      id="date"
                      label="Ngày bắt đầu"
                      type="date"
                      value={experience.starting}
                      defaultValue={experience.starting}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e: any) => {
                        changeExperienceTab(index, { 
                          starting: e.target.value,
                          ending: experience.ending,
                          unit: experience.unit,
                          description: experience.description,
                          level: experience.level,
                          position: experience.position });

                        onChangeExperience(getCurrentExperience())
                      }}
                    />
                  </CardContent>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <CardHeader
                      className={classes.cardHeader}
                      title={
                        <>
                          Ngày kết thúc 
                        </>
                      }
                    />
                  <CardContent className={classes.cardContent}>
                    <Alert severity={"info"}>Ngày kết thúc</Alert>
                    <TextField
                      id="date"
                      label="Ngày kết thúc"
                      type="date"
                      value={experience.ending}
                      defaultValue={experience.ending}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e: any) => {
                        changeExperienceTab(index, { 
                          starting: experience.starting,
                          ending: e.target.value,
                          unit: experience.unit,
                          description: experience.description,
                          level: experience.level,
                          position: experience.position })

                        onChangeExperience(getCurrentExperience())
                      }}
                    />
                  </CardContent>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Vị trí công việc 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Vị trí công việc</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={experience.position}
                    onBlur={() => onChangeExperience(getCurrentExperience())}
                    onChange={(e: any) => changeExperienceTab(index, { 
                      starting: experience.starting,
                      ending: experience.ending,
                      unit: experience.unit,
                      description: experience.description,
                      level: experience.level,
                      position: e.target.value })}
                    label={"Vị trí công việc"}
                  />
                </CardContent>
              </Grid>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Cấp bậc công việc 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Cấp bậc công việc</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={experience.level}
                    onBlur={() => onChangeExperience(getCurrentExperience())}
                    onChange={(e: any) => changeExperienceTab(index, { 
                      starting: experience.starting,
                      ending: experience.ending,
                      unit: experience.unit,
                      description: experience.description,
                      level: e.target.value,
                      position: experience.position })}
                    label={"Cấp bậc công việc"}
                  />
                </CardContent>
              </Grid>
              <Grid xs={12} className={classes.card}>
                <CardHeader
                  className={classes.cardHeader}
                  title={
                    <>
                      Mô tả chi tiết công việc 
                    </>
                  }
                />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Mô tả chi tiết công việc</Alert>
                  <TextField
                    multiline
                    rows={6}
                    value={experience.description}
                    label="Mô tả chi tiết công việc"
                    variant="outlined"
                    fullWidth
                    onBlur={() => onChangeExperience(getCurrentExperience())}
                    onChange={(e: any) => changeExperienceTab(index, { 
                      starting: experience.starting,
                      ending: experience.ending,
                      unit: experience.unit,
                      description: e.target.value,
                      level: experience.level,
                      position: experience.position })}
                  />
                </CardContent>
                <div className={classes.btnRemove}>
                  <Button color="primary" onClick={ () => handleRemoveItem(index, "EXPERIENCE") } variant="contained">
                    Xóa
                  </Button>
                </div>
              </Grid>
            </Grid>
          ))
        }
        <Grid xs={12}>
          <Button className={classes.btnAdd} variant="contained" color="primary" onClick={handleAddExperience}>
            Thêm kinh nghiệm làm việc
          </Button>
        </Grid>
      </Grid>
      {/* // Handle Project */}
      <Grid container xs={12} className={classes.card}>
        <Grid xs={12}>
          <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Các dự án đã tham gia 
              </>
            }
          />
        </Grid>
        {
          projects.map((project, index) => (
            <Grid key={index} container className={classes.itemContent} xs={12}>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Tên dự án  
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Tên dự án</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={project.name || ""}
                    onBlur={() => onChangeExperience(getCurrentExperience())}
                    onChange={(e: any) => onChangeProject(index, { 
                      starting: project.starting,
                      ending: project.ending,
                      name: e.target.value,
                      description: project.description,
                      position: project.position })}
                    label={"Tên dự án"}
                  />
                </CardContent>
              </Grid>
              <Grid container xs={12} sm={6} className={classes.card}>
                <Grid item xs={12} sm={6}>
                  <CardHeader
                      className={classes.cardHeader}
                      title={
                        <>
                          Ngày bắt đầu 
                        </>
                      }
                    />
                  <CardContent className={classes.cardContent}>
                    <Alert severity={"info"}>Ngày bắt đầu</Alert>
                    <TextField
                      id="date"
                      label="Ngày bắt đầu"
                      type="date"
                      value={project.starting}
                      defaultValue={project.starting}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e: any) => {
                        onChangeProject(index, { 
                          starting: e.target.value,
                          ending: project.ending,
                          name: project.name,
                          description: project.description,
                          position: project.position });

                        onChangeExperience(getCurrentExperience())
                      }}
                    />
                  </CardContent>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <CardHeader
                      className={classes.cardHeader}
                      title={
                        <>
                          Ngày kết thúc 
                        </>
                      }
                    />
                  <CardContent className={classes.cardContent}>
                    <Alert severity={"info"}>Ngày kết thúc</Alert>
                    <TextField
                      id="date"
                      label="Ngày kết thúc"
                      type="date"
                      value={project.ending}
                      defaultValue={project.ending}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e: any) => {
                        onChangeProject(index, { 
                          starting: project.starting,
                          ending: e.target.value,
                          name: project.name,
                          description: project.description,
                          position: project.position });

                        onChangeExperience(getCurrentExperience())
                      }}
                    />
                  </CardContent>
                </Grid>
              </Grid>
              <Grid item xs={12} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Vị trí công việc 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Vị trí công việc</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={project.position}
                    onBlur={() => onChangeExperience(getCurrentExperience())}
                    onChange={(e: any) => onChangeProject(index, { 
                      starting: project.starting,
                      ending: project.ending,
                      name: project.name,
                      description: project.description,
                      position: e.target.value })}
                    label={"Vị trí công việc"}
                  />
                </CardContent>
              </Grid>
              <Grid xs={12} className={classes.card}>
                <CardHeader
                  className={classes.cardHeader}
                  title={
                    <>
                      Mô tả chi tiết công việc 
                    </>
                  }
                />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Mô tả chi tiết công việc</Alert>
                  <TextField
                    multiline
                    rows={6}
                    value={project.description}
                    label="Mô tả chi tiết công việc"
                    variant="outlined"
                    fullWidth
                    onBlur={() => onChangeExperience(getCurrentExperience())}
                    onChange={(e: any) => onChangeProject(index, { 
                      starting: project.starting,
                      ending: project.ending,
                      name: project.name,
                      description: e.target.value,
                      position: project.position })}
                  />
                </CardContent>
                <div className={classes.btnRemove}>
                  <Button color="primary" onClick={ () => handleRemoveItem(index, "PROJECT") } variant="contained">
                    Xóa
                  </Button>
                </div>
              </Grid>
            </Grid>
          ))
        }
        <Grid xs={12}>
          <Button className={classes.btnAdd} variant="contained" color="primary" onClick={handleAddProject}>
            Thêm dự án
          </Button>
        </Grid>
      </Grid>
      {/* // Handle Skill */}
      <Grid container xs={12} className={classes.card}>
        <Grid xs={12}>
          <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Các dự án đã tham gia 
              </>
            }
          />
        </Grid>
        {
          skills.map((skill, index) => (
            <Grid key={index} container className={classes.itemContent} xs={12}>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Tên kỹ năng  
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Tên kỹ năng</Alert>
                  <TextField
                    variant="outlined"
                    fullWidth
                    value={skill.name || ""}
                    onBlur={() => onChangeExperience(getCurrentExperience())}
                    onChange={(e: any) => changeSkillTab(index, { 
                      name: e.target.value,
                      description: skill.description,
                      point: skill.point })}
                    label={"Tên kỹ năng"}
                  />
                </CardContent>
              </Grid>
              <Grid item xs={12} sm={6} className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title={
                      <>
                        Thang điểm 
                      </>
                    }
                  />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Thang điểm của kỹ năng [0:100]</Alert>
                  <Slider
                    defaultValue={30}
                    aria-labelledby="discrete-slider"
                    valueLabelDisplay="auto"
                    step={5}
                    marks={marksLevelSkill}
                    min={0}
                    max={100}
                    onChange={(e: any, value: any) => {
                      changeSkillTab(index, { 
                        name: skill.name,
                        description: skill.description,
                        point: value });

                      onChangeExperience(getCurrentExperience())
                    }}
                  />
                </CardContent>
              </Grid>
              <Grid xs={12} className={classes.card}>
                <CardHeader
                  className={classes.cardHeader}
                  title={
                    <>
                      Mô tả chi tiết kỹ năng 
                    </>
                  }
                />
                <CardContent className={classes.cardContent}>
                  <Alert severity={"info"}>Mô tả chi tiết kỹ năng</Alert>
                  <TextField
                    multiline
                    rows={4}
                    value={skill.description}
                    label="Mô tả chi tiết kỹ năng"
                    variant="outlined"
                    fullWidth
                    onBlur={() => onChangeExperience(getCurrentExperience())}
                    onChange={(e: any) => changeSkillTab(index, { 
                      name: skill.name,
                      description: e.target.value,
                      point: skill.point })}
                  />
                </CardContent>
                <div className={classes.btnRemove}>
                  <Button color="primary" onClick={ () => handleRemoveItem(index, "SKILL") } variant="contained">
                    Xóa
                  </Button>
                </div>
              </Grid>
            </Grid>
          ))
        }
        <Grid xs={12}>
          <Button className={classes.btnAdd} variant="contained" color="primary" onClick={handleAddSkill}>
            Thêm kỹ năng
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default TabExperience;
