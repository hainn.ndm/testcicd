import React, { ComponentType, useState, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchCity, fetchDistrict, fetchWard } from "store/redux/actions/AddressAction";

import makeStyles from "@material-ui/core/styles/makeStyles";
import { Theme } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import Button from "@material-ui/core/Button";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CircularProgress from '@material-ui/core/CircularProgress';
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { axios } from "utils/axiosInstance";
import { getBase64 } from "helpers/file";
import { API } from "utils/api";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    paddingBottom: "50px"
  },
  card: {
    backgroundColor: "#fff",
    minHeight: "15px",
  },
  cardHeader: {
    padding: `${theme.spacing(3)}px ${theme.spacing(1)}px ${theme.spacing(1)}px ${theme.spacing(1)}px`,
    backgroundColor: "#fff",
  },
  cardContent: {
    padding: `0px ${theme.spacing(1)}px`,
    paddingBottom: `0 !important`,
  },
  required: {
    color: "red",
  },
  choiceSex: {
    display: "flex",
    paddingTop: theme.spacing(1)
  },
  btnAdd: {
    margin: "15px 0 0 8px",
  }
}));

interface IProps {
  onChangeInfo: (info: any) => void;
}

const TabInformation: ComponentType<IProps> = (props: IProps) => {
  const classes = useStyles();
  const { onChangeInfo } = props;
  
  const dispatch = useDispatch();

  const refFileCV = useRef<any>(null);
  const [file, setFile] = useState<string>("");
  const [city, setCity] = useState<any>();
  const [district, setDistrict] = useState<any>();
  const [ward, setWard] = useState<any>();
  const [addressText, setAddressText] = useState<string>("");
  const [specificAddress, setSpecificAddress] = useState("");
  const [sex, setSex] = useState<string>("nam");
  const [phone, setPhone] = useState<string>("");
  const [birthday, setBirthday] = useState<Date>(new Date("1999-12-27"));
  const [email, setEmail] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [facebook, setFacebook] = useState<string>("");
  const [linkedin, setLinkedin] = useState<string>("");
  const [website, setWebsite] = useState<string>("");
  const [infoMore, setInfoMore] = useState<string>("");
  const [preference, setPreference] = useState<string>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    // handle cache
    if (window.localStorage) {
      //@ts-ignore
      const cvInfo: any = JSON.parse(window.localStorage.getItem("cvInformation"))
      if (cvInfo) {
        setSex(cvInfo.sex || "other")
        setBirthday(cvInfo.birthday || null)
        setPhone(cvInfo.phone || "")
        setEmail(cvInfo.email || "")
        setName(cvInfo.name || "")
        setFacebook(cvInfo.facebook || "")
        setLinkedin(cvInfo.linkedin || "")
        setWebsite(cvInfo.website || "")
        setSpecificAddress(cvInfo.specificAddress || "")
        setAddressText(cvInfo.addressText || "")
        setInfoMore(cvInfo.infoMore || "")
        setPreference(cvInfo.preference || "")

        if (cvInfo.city && cvInfo.district && cvInfo.ward) {
          setCity(cvInfo.city)
          setDistrict(cvInfo.district)
          setWard(cvInfo.ward)
        }
      }
    }

    // handle fetch city
    dispatch(fetchCity({
      variables: {
        page: 1,
        limit: 70,
        filter: "{}",
      },
    }));
  }, []);

  const { address } = useSelector((state: any) => state);

  const getCurrentInfo = () => {
    const info: any = {
      city: city,
      district: district,
      ward: ward,
      addressText: addressText,
      specificAddress: specificAddress,
      sex: sex,
      phone: phone,
      birthday: birthday,
      email: email,
      name: name,
      facebook: facebook,
      linkedin: linkedin,
      website: website,
      infoMore: infoMore,
      preference: preference,
      file: file,
    }
    
    if (window.localStorage) {
      window.localStorage.setItem("cvInformation", JSON.stringify(info))
    }

    return info
  }

  const handleChangeCity = (value: any) => {
    setCity(value);
    setDistrict(null)
    setWard(null);
    if (value) {
      dispatch(fetchDistrict({
        variables: {
          page: 1,
          limit: 30,
          filter: `{'city': '${value.node._id}'}`,
        },
      }));
      setAddressText(value.node.name);
    }
  };

  const handleChangeDistrict = (value: any) => {
    setDistrict(value);
    setWard(null)
    if (value) {
      dispatch(fetchWard({
        variables: {
          page: 1,
          limit: 30,
          filter: `{'district': '${value.node._id}'}`,
        },
      }));
      setAddressText(`${value.node.name}, ${city.node.name}`);
    }
  };
  const handleChangeWard = (value: any) => {
    setWard(value);
    if (value) {
      setAddressText(`${value.node.name}, ${district.node.name}, ${city.node.name}`);
    }
  };

  const handleChangeSpecificAddress = (value: any) => {
    setSpecificAddress(value);
    let addr = "";
    if (ward) {
      if (district) {
        if (city) {
          addr = `${ward.node.name}, ${district.node.name}, ${city.node.name}`;
        }
      } else {
        addr = `${city.node.name}`;
      }
    } else {
      addr = `${district.node.name}, ${city.node.name}`;
    }

    if (value) {
      if (addr) {
        addr = `${value}, ${addr}`;
      } else {
        addr = `${value}`;
      }
    }
    setAddressText(addr);
  };

  const handleUploadFile = async (e: any) => {
    setIsLoading(true)
    const fileTemp = await getBase64(e.target.files[0])
    axios.post(`${API.UP_LOAD_FILE_CV_BASE64}`, {
      files: [fileTemp]
    }).then(r => {
      if (r && r.data) {
        setFile(r.data.urls[0])
        onChangeInfo({
          city: city,
          district: district,
          ward: ward,
          addressText: addressText,
          specificAddress: specificAddress,
          sex: sex,
          phone: phone,
          birthday: birthday,
          email: email,
          name: name,
          facebook: facebook,
          linkedin: linkedin,
          website: website,
          infoMore: infoMore,
          preference: preference,
          file: r.data.urls[0],
        })
      }
      setIsLoading(false)
    }).catch(e => {
      setIsLoading(false)
      alert("Tải file lỗi")
    })
  }

  return (
    <Grid className={classes.root} container item xs={12}>
      <Grid item xs={12} sm={6} className={classes.card}>
        <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Họ và tên <span className={classes.required}>*</span>
              </>
            }
          />
        <CardContent className={classes.cardContent}>
          <Alert severity={"info"}>Họ và tên đầy đủ của ứng viên</Alert>
          <TextField
            variant="outlined"
            fullWidth
            autoFocus={true}
            value={name}
            onBlur={() => onChangeInfo(getCurrentInfo())}
            onChange={(e: any) => {
              setName(e.target.value)
            }}
            label={"Họ và tên"}
          />
        </CardContent>
      </Grid>
      <Grid item xs={12} sm={6} className={classes.card}>
        <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Email <span className={classes.required}>*</span>
              </>
            }
          />
        <CardContent className={classes.cardContent}>
          <Alert severity={"info"}>Liên hệ qua hòm thư điện tử</Alert>
          <TextField
            variant="outlined"
            fullWidth
            value={email}
            onBlur={() => onChangeInfo(getCurrentInfo())}
            onChange={(e: any) => {
              setEmail(e.target.value)
            }}
            label={"Địa chỉ email"}
          />
        </CardContent>
      </Grid>
      <Grid container xs={12} item sm={6} className={classes.card}>
        <Grid item xs={12} sm={6}>
          <CardHeader
              className={classes.cardHeader}
              title={
                <>
                  Ngày sinh <span className={classes.required}>*</span>
                </>
              }
            />
          <CardContent className={classes.cardContent}>
            <Alert severity={"info"}>Sinh nhật</Alert>
            <TextField
              id="date"
              label="Ngày sinh"
              type="date"
              value={birthday}
              defaultValue={birthday}
              fullWidth
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e: any) => {
                  setBirthday(e.target.value)
                  onChangeInfo(getCurrentInfo())
                }
              }
            />
          </CardContent>
        </Grid>
        <Grid item xs={12} sm={6}>
          <CardHeader
              className={classes.cardHeader}
              title={
                <>
                  Giới tính <span className={classes.required}>*</span>
                </>
              }
            />
          <CardContent className={classes.cardContent}>
            <Alert severity={"info"}>Giới tính</Alert>
            <FormControl component="fieldset">
              <RadioGroup
                aria-label="gender"
                name="gender1"
                value={sex}
                onChange={(event: any) => {
                  setSex(event.target.value)
                  onChangeInfo(getCurrentInfo())
                }}
              >
                <div className={classes.choiceSex}>
                  <FormControlLabel value="nu" control={<Radio />} label="Nữ" />
                  <FormControlLabel value="nam" control={<Radio />} label="Nam" />
                </div>
              </RadioGroup>
            </FormControl>
          </CardContent>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={6} className={classes.card}>
        <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Facebook 
              </>
            }
          />
        <CardContent className={classes.cardContent}>
          <Alert severity={"info"}>Liên hệ qua MXH Facebook</Alert>
          <TextField
            variant="outlined"
            fullWidth
            value={facebook}
            onBlur={() => onChangeInfo(getCurrentInfo())}
            onChange={(e: any) => {
              setFacebook(e.target.value)
            }}
            label={"Địa chỉ facebook"}
          />
        </CardContent>
      </Grid>
      <Grid item xs={12} sm={6} className={classes.card}>
        <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Số điện thoại <span className={classes.required}>*</span>
              </>
            }
          />
        <CardContent className={classes.cardContent}>
          <Alert severity={"info"}>Liên hệ qua SĐT cá nhân</Alert>
          <TextField
            variant="outlined"
            fullWidth
            value={phone}
            onBlur={() => onChangeInfo(getCurrentInfo())}
            onChange={(e: any) => {
              setPhone(e.target.value)
            }}
            label={"Số điện thoại"}
          />
        </CardContent>
      </Grid>
      <Grid item xs={12} sm={6} className={classes.card}>
        <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Linkedin 
              </>
            }
          />
        <CardContent className={classes.cardContent}>
          <Alert severity={"info"}>Liên hệ qua Linkedin</Alert>
          <TextField
            variant="outlined"
            fullWidth
            value={linkedin}
            onBlur={() => onChangeInfo(getCurrentInfo())}
            onChange={(e: any) => {
              setLinkedin(e.target.value)
            }}
            label={"Địa chỉ linkedin"}
          />
        </CardContent>
      </Grid>
      <Grid item xs={12} className={classes.card}>
        <CardHeader
            className={classes.cardHeader}
            title={
              <>
                Địa chỉ website 
              </>
            }
          />
        <CardContent className={classes.cardContent}>
          <Alert severity={"info"}>Liên hệ website</Alert>
          <TextField
            variant="outlined"
            fullWidth
            value={website}
            onBlur={() => onChangeInfo(getCurrentInfo())}
            onChange={(e: any) => {
              setWebsite(e.target.value)
            }}
            label={"Địa chỉ website"}
          />
        </CardContent>
      </Grid>
      <Grid item xs={12}>
        <CardHeader
          className={classes.cardHeader}
          title={
            <>
              Địa chỉ nơi ở 
            </>
          }
        />
        <CardContent className={classes.cardContent}>
          <Alert severity={"info"}>Địa chỉ nơi ở của ứng viên</Alert>
          <Grid container spacing={2}>
            <Grid item xs={12} md={4}>
              <Autocomplete
                fullWidth
                value={city}
                options={address.city}
                onChange={(event: any, value: any) => handleChangeCity(value)}
                getOptionLabel={(option) => option.node.name}
                renderInput={(params) => (
                  <TextField {...params} label={"Tỉnh/Thành phố"} variant="outlined" fullWidth />
                )}
              />
            </Grid>
            <Grid item xs={12} md={4}>
              <Autocomplete
                fullWidth
                disabled={!city}
                value={district}
                options={address.district}
                onChange={(event: any, value: any) => {
                  handleChangeDistrict(value)
                  onChangeInfo(getCurrentInfo())
                }}
                getOptionLabel={(option) => option.node.name}
                renderInput={(params) => (
                  <TextField {...params} label={"Quận/Huyện"} variant="outlined" fullWidth />
                )}
              />
            </Grid>
            <Grid item xs={12} md={4}>
              <Autocomplete
                fullWidth
                disabled={!district}
                value={ward}
                options={address.ward}
                onChange={(event: any, value: any) => {
                  handleChangeWard(value)
                  onChangeInfo(getCurrentInfo())
                }}
                getOptionLabel={(option) => option.node.name}
                renderInput={(params) => (
                  <TextField {...params} label={"Xã/Phường"} variant="outlined" fullWidth />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label={"Địa chỉ cụ thể (số nhà, hẻm, ngõ, ngách, toà nhà)"}
                variant="outlined"
                fullWidth
                value={specificAddress}
                onChange={(e) => {
                  handleChangeSpecificAddress(e.target.value);
                  onChangeInfo(getCurrentInfo())
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label={"Địa chỉ được hiển thị"}
                variant="outlined"
                fullWidth
                disabled
                value={addressText}
                onChange={(e) => {
                  setAddressText(e.target.value);
                }}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Grid>
      <Grid item xs={12} className={classes.card}>
        <CardHeader
          className={classes.cardHeader}
          title={
            <>
              Thông tin thêm 
            </>
          }
        />
        <CardContent className={classes.cardContent}>
          <Alert severity={"info"}>Mô tả thêm thông tin</Alert>
          <TextField
            multiline
            rows={6}
            value={infoMore}
            label="Mô tả thêm thông tin"
            variant="outlined"
            fullWidth
            onBlur={() => onChangeInfo(getCurrentInfo())}
            onChange={(event) => {
              setInfoMore(event.target.value)
            }}
          />
        </CardContent>
      </Grid>
      <Grid item xs={12} className={classes.card}>
        <CardHeader
          className={classes.cardHeader}
          title={
            <>
              Sở thích 
            </>
          }
        />
        <CardContent className={classes.cardContent}>
          <Alert severity={"info"}>Sở thích cá nhân</Alert>
          <TextField
            multiline
            rows={6}
            value={preference}
            label="Sở thích cá nhân"
            variant="outlined"
            fullWidth
            onBlur={() => onChangeInfo(getCurrentInfo())}
            onChange={(event) => {
              setPreference(event.target.value)
            }}
          />
        </CardContent>
      </Grid>
      <Grid item xs={12}>
        <input ref={refFileCV}
          onChange={(e) => handleUploadFile(e)}
          style={{ display: "none" }} name="userfile"
          type="file"
        />
        {
          isLoading ? (
            <Button className={classes.btnAdd} variant="contained" color="primary">
              <CircularProgress color="inherit" />
            </Button>
          ) : (
            <>
              <Button className={classes.btnAdd} variant="contained" color="primary" onClick={() => refFileCV.current.click()}>
                Thêm file CV
              </Button>
              <Typography className={classes.btnAdd}>
                { file ? file : "Chọn file CV của ứng viên" }
              </Typography>
            </>
          )
        }
      </Grid>
    </Grid>
  );
};

export default TabInformation;
