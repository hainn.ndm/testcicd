import CVType from "store/redux/types/CVType";

export const createCV = (payload: any) => ({
  type: CVType.CREATE_CV,
  payload,
});

export const createCVSucceed = () => ({
  type: CVType.CREATE_CV_SUCCEED,
});

export const createCVFailed = (payload: any) => ({
  type: CVType.CREATE_CV_FAILED,
  payload,
});

export const fetchCV = (payload: any) => ({
  type: CVType.FETCH_CV,
  payload,
});

export const fetchCVSucceed = (payload: any) => ({
  type: CVType.FETCH_CV_SUCCEED,
  payload,
});

export const fetchCVFailed = () => ({
  type: CVType.FETCH_CV_FAILED
});


export const updateCV = (payload: any) => ({
  type: CVType.UPDATE_CV,
  payload,
});

export const updateCVSucceed = (payload: any) => ({
  type: CVType.UPDATE_CV_SUCCEED,
  payload,
});

export const updateCVFailed = () => ({
  type: CVType.UPDATE_CV_FAILED,
});


