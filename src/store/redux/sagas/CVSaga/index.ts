import { fetchCVFailed, updateCVFailed } from "../../actions/CVAction/index";
import CVType from "store/redux/types/CVType";
import { axios } from "utils/axiosInstance";

import { all, fork, put, takeLatest } from "redux-saga/effects";
import {
  createCVFailed,
  createCVSucceed,
  fetchCVSucceed,
  updateCVSucceed,
} from "store/redux/actions/CVAction";
import { enqueueSnackbar } from "store/redux/actions/GlobalToastAction";
import { closeModal } from "store/redux/actions/GlobalModalAction";

function* createCVSaga(action: any) {
  try {
    yield axios.post(`${process.env.DEV_CV_DOMAIN}/cv/create`, { data: action.payload })
    const toast = {
      message: "Cập nhật thành công",
      options: {
        key: new Date().getTime() + Math.random(),
        variant: "success",
      },
    };
    yield put(closeModal());
    yield put(enqueueSnackbar(toast));
    yield alert("Tạo CV thành công");
    yield put(createCVSucceed());
    yield (window.location.href = `/cv/list`);
  } catch (e) {
    const toast = {
      message: "Lỗi cập nhât máy chủ",
      options: {
        key: new Date().getTime() + Math.random(),
        variant: "error",
      },
    };
    yield put(closeModal());
    yield put(enqueueSnackbar(toast));
    yield put(createCVFailed(e));
  }
}

function* fetchCVSaga(action: any) {
  try {
    let r = yield axios.get(`${process.env.DEV_CV_DOMAIN}/cv/${action.payload.id}`)
    yield put(fetchCVSucceed({ detail: r.data.CV }));
  } catch (e) {
    console.log(e);
    yield put(fetchCVFailed());
  }
}

function* updateCVSaga(action: any) {
  try {
    let r = yield axios.put(`${process.env.DEV_CV_DOMAIN}/cv/update`, { data: action.payload })
    const toast = {
      message: "Cập nhật thành công",
      options: {
        key: new Date().getTime() + Math.random(),
        variant: "success",
      },
    };
    yield put(closeModal());
    yield put(enqueueSnackbar(toast));
    yield alert("Cập nhật CV thành công");
    yield put(updateCVSucceed({ detail: r.data.CVUpdate }));
  } catch (e) {
    const toast = {
      message: "Lỗi cập nhât máy chủ",
      options: {
        key: new Date().getTime() + Math.random(),
        variant: "error",
      },
    };
    yield put(closeModal());
    yield put(enqueueSnackbar(toast));
    yield put(updateCVFailed());
  }
}

function* watchFetchCVSaga() {
  yield takeLatest([CVType.CREATE_CV], createCVSaga);
  yield takeLatest(CVType.FETCH_CV, fetchCVSaga);
  yield takeLatest([CVType.UPDATE_CV], updateCVSaga);
}

function* CVSaga() {
  yield all([fork(watchFetchCVSaga)]);
}

export default CVSaga;
