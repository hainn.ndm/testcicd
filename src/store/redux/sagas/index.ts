import { all, fork } from "redux-saga/effects";
import addressSaga from "store/redux/sagas/AddressSaga";


function* rootSaga() {
  yield all([
    fork(addressSaga),
  ]);
}

export default rootSaga;
