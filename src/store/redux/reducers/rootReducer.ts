import { combineReducers } from "redux";
import { GlobalToastReducer } from "store/redux/reducers/GlobalToastReducer";
import { GlobalModalReducer } from "store/redux/reducers/GlobalModalReducer";
import { AddressReducer } from "store/redux/reducers/AddressReducer";
import { CVReducer } from "store/redux/reducers/CVReducer";

const rootReducer = combineReducers({
  globalToast: GlobalToastReducer,
  globalModal: GlobalModalReducer,
  address: AddressReducer,
  CV: CVReducer,
});

export default rootReducer;
