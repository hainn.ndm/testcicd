import HomeIcon from "@material-ui/icons/HomeOutlined";
import ChromeReaderModeIcon from '@material-ui/icons/ChromeReaderMode';

export default [
  {
    title: "Bảng điều khiển",
    pages: [
      {
        title: "Tổng quan",
        href: "/overview",
        icon: HomeIcon,
      },
    ],
  },
  {
    title: "Quản lý",
    pages: [
      {
        title: "CV ứng viên",
        href: "/cv",
        icon: ChromeReaderModeIcon,
        children: [
          {
            title: "Nhập CV",
            href: "/cv/create",
          },
          {
            title: "Tất cả",
            href: "/cv/list",
          }
        ]
      }
    ]
  }
];
