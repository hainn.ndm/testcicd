import React, { lazy } from "react";
import { Redirect } from "react-router-dom";

const routes = [
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/overview" />,
  },
  {
    path: `/overview`,
    exact: true,
    component: lazy(() => import("views/Overview")),
  },
  {
    path: "/cv/create",
    exact: true,
    component: lazy(() => import("views/CVCreate/CVCreate")),
  },
  {
    path: "/cv/list",
    exact: true,
    component: lazy(() => import("views/CVList/CVList")),
  },
  {
    path: "/cv/:slug",
    exact: true,
    component: lazy(() => import("views/CVEdit/Edit")),
  }
];

export default routes;
