// const CV_BACKEND_URL = process.env.REACT_APP_CV_API_URL;
const CV_BACKEND_URL = "https://32114f266120.ngrok.io/api";

export const API = {
  CREATE_CV: `${CV_BACKEND_URL}/base-informations`,
  UPDATE_CV_BY_ID: `${CV_BACKEND_URL}/base-informations`,
  GET_CV_BY_ID: `${CV_BACKEND_URL}/base-informations`,
  GET_CV_LIST: `${CV_BACKEND_URL}/base-informations`,
  UP_LOAD_FILE_CV_BASE64: `${CV_BACKEND_URL}/upload-files/base64`,
}
