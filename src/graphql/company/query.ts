import gql from "graphql-tag";

export const COMPANY_COUNT = gql`
  query companys($page: Int!, $limit: Int!, $filter: String!) {
    companys(page: $page, filter: $filter, limit: $limit) {
      edges {
        node {
          _id
        }
      }
      pageInfo {
        length
      }
    }
  }
`;
