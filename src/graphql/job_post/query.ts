import gql from "graphql-tag";

export const JOB_POST_COUNT = gql`
  query jobPosts($page: Int!, $limit: Int!, $filter: String!) {
    jobPosts(page: $page, filter: $filter, limit: $limit) {
      edges {
        node {
          _id
        }
      }
      pageInfo {
        length
      }
    }
  }
`;
