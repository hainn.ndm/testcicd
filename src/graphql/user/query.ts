import gql from "graphql-tag";

export const USERS_COUNT = gql`
  query users($page: Int!, $limit: Int!, $filter: String!) {
    users(page: $page, filter: $filter, limit: $limit) {
      pageInfo {
        length
      }
    }
  }
`;
