import moment from "moment";

export const timestampDateFormat = (timestamp: number) => moment(timestamp).format("DD/MM/YYYY");

export const timestampDateFormat2 = (timestamp: number) => moment(timestamp).format("YYYY-MM-DD");

export const timestampDateTimeFormat = (timestamp: number) => moment(timestamp).format("HH:mm:ss DD/MM/YYYY");
